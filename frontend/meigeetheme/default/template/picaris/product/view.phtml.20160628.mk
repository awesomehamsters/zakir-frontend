<?php
$tproduct= Mage::getModel('catalog/product')->load($this->getProduct()->getId());
$sproduct = Mage::getModel('catalog/product')->load($this->getProduct()->getId());
$custdata = Mage::helper('picaris')->collectProductData($tproduct->getData('entity_id'));
$ignoredPrices = explode(',',$tproduct->getData('picaris_product_ignore_price'));
?>
<script type="text/javascript" src="http://clseifert.com/files/unity/latest/storage.js"></script>
<script type="text/javascript" src="http://clseifert.com/files/unity/latest/unityUtil.js"></script>
<script type="text/javascript" src="http://clseifert.com/files/unity/latest/unityUtilExt.js"></script>
<script type="text/javascript">
    picarisprodattr.mage = <?php echo json_encode($tproduct->getData()); ?>;
    picarisprodattr.cust =  <?php echo json_encode($custdata); ?>;
    picarisprodattr.custmap = [];
    picarisprodattr.cmds = [];
    picarisprodattr.picarisImageView = [];
    picarisprodattr.threeDView = [];
    picarisprodattr.threeDView.eventInitializeHat = jQuery.Deferred();
    custAttributesMap();
    picarisIgnorePriceProcess();
    picarisprodattr.picarisImageView['enabled'] = <?php if(Mage::getStoreConfig('picaris/picaris_run_group/useproductimpicaris')){ ?> true <?php }else{ ?> false <?php } ?>;
    picarisprodattr.threeDView['enabled'] = <?php if(Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3d')){ ?> true <?php }else{ ?> false <?php } ?>;
    picarisprodattr.currencySymbol = '<?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>';
    picarisprodattr.firstloaddone = false;
    picarisprodattr.lockcamera = false;
    picarisprodattr.needReinit = false;
    picarisprodattr.noUnity3DPluginText = <?php echo "'".Mage::helper('picaris')->__('Unity 3D is required. Please turn on <a target="_blank" class="unity3dlink" href="http://unity3d.com/webplayer">Unity 3D</a> plugin')."'"; ?>;
    picarisprodattr.threeDView.unity3DPlayer = function(){
       new Ajax.Updater('unitywrapper-window', '<?php echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB); ?>default/unitywrapper/?id='+picarisprodattr.mage['entity_id'], {
			method: 'get',
                        onLoading: function() {},
                        async: true,
			evalScripts: true,
			onComplete: function() {
                            if (typeof unityObject !== "undefined")
                            {
                                unityObject.embedUnity("unityPlayer", "<?php echo $sproduct->getData('picaris_unity3d_url');  ?>", jQuery('.picaris-product-page-content-mid').css('width'), Math.round(jQuery('.picaris-product-page-content-mid').width() * 0.75));
                                jQuery('#unitywrapper-window').css({'display':'block'});
                            }
                            /*
                            setTimeout(function(){
                                try{
                                    sendUnityMessage("<msg type='Debug' debugPanel='false' />");
                                }catch(e){
                                    jQuery('#product-img-useproductimunity3d').html('<h3 style="color:#ff0000; position:relative; top:150px; text-align:center; margin:0 auto;">' + picarisprodattr.noUnity3DPluginText + '</h3>');
                                }
                            },500);
                            */
                        }
                    });
    };
    
    function initializeHat() 
    {
        showDebugPanel(false);
        setSceneCurtain(true);
        setSimpleQualityLevel();
        sendUnityMessage("<msg type='LoadAsset' cleanCache='true' url='Unity/assets/<?php echo $tproduct->getAttributeText('hat_model'); ?>.unity3d' asset='<?php echo $tproduct->getAttributeText('hat_model'); ?>' />");
        showModelByName('<?php echo $tproduct->getAttributeText('hat_model'); ?>');
        initializeHatOptions();
        picarisprodattr.threeDView.eventInitializeHat.resolve(1);
        <?php
        $unity3DInitMessages = explode(',',$tproduct->getData('picaris_unity3d_init'));
        foreach($unity3DInitMessages as $unity3DMsg){
            ?>
              sendUnityMessage("<?php echo trim($unity3DMsg) ?>");
            <?php
        }
        ?>
        setCameraRotationEnabled(true);
        setCameraZoomEnabled(true);
        setCameraZoom(22);
        setCameraRotation(160, 0);
        setSceneCurtain(false);
        setQueueEnabled(false);
        startQueue();
        
        jQuery('#preload-prompt-no-unityplayer').remove();
    }
    
    function sendUnityMessage(message,attribute_id)
    {

        if(arguments.length > 1){
            console.log(attribute_id + "::" + message );
        }
        else{
            console.log( "1.1.9: " + message );
        }

        message = message.replace(/\"/g, "'");

        if ( message.indexOf("hagerem") > 0 ) {return}

        var unity = getUnity();

        // Send message to unity

        if (unity != null){
                unity.SendMessage("System", "ParseMessage", message);
        }

    }
    
    jQuery(document).ready(function(){
      
        var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
      
        picarisprodattr.unity3DPluginChecker = setInterval(function(){
            if(jQuery('#unityPlayer_img').length){
                //unity3d not available
                jQuery('#product-img-useproductimunity3d').html('<h3 style="color:#ff0000; position:relative; top:150px; text-align:center; margin:0 auto;">' + picarisprodattr.noUnity3DPluginText + '</h3>');
                
                
                
                if(is_chrome){
                    jQuery('#product-img-useproductimunity3d .chrome').css('display','inline-block');
                    jQuery('#product-img-useproductimunity3d .not_chrome').css('display','none');
                }
                else{
                    jQuery('#product-img-useproductimunity3d .chrome').css('display','none');
                }
                
                delete picarisprodattr.unity3DPluginChecker;
            }
			
        },250);
        
        if(is_chrome){
            jQuery('#product-img-useproductimunity3d .chrome').css('display','inline-block');
            jQuery('#product-img-useproductimunity3d .not_chrome').css('display','none');
        }
        else{
            jQuery('#product-img-useproductimunity3d .chrome').css('display','none');
 
        }
        
    });
    
</script>

<form id="picaris-custom-options-form" method="post" action="<?php echo Mage::getBaseUrl(); ?>picaris/index/addtocart">
    <input type="hidden" name="sku" value="<?php echo $tproduct->getData('sku'); ?>" style="display:none;">
    <div class="picaris-product-page">
        <div class="picaris-product-page-content">
            <div class="picaris-product-package-options-text">
                    <?php if(!empty($tproduct['picaris_product_intro_header'])){ ?>
                    <h2 style="font-weight:bold; font-family:raleway;"><?php echo $tproduct['picaris_product_intro_header']; ?></h2>
                    <?php } ?>
                    
                    <?php if(!empty($tproduct['picaris_product_intro_content'])){ ?>
                    <p style='font-family:raleway;'><?php echo $tproduct['picaris_product_intro_content']; ?></p>
                    <?php } ?>
                </div>
            <div class="picaris-product-page-content-left">
                <div class="picaris-product-page-content-groupcontainter">
                    <ul class="picaris-product-page-content-option-groups">
                        <?php if(count($custdata) <= 0){
                            ?>
                            <script type="text/javascript">
                                alert('Error Occured. Please create groups for this product');
                            </script>
                            <?php
                        } ?>
                        <?php
                        foreach($custdata as $group){
                            ?>
                                <li data-isvisited="0" data-threedcamera="<?php echo $group['threed_camera']; ?>" data-group-id="<?php echo $group['group_id']; ?>" class="picaris-group-button" id="group-btn-<?php echo $group['group_id']; ?>">
                                    <span style="visibility:hidden;" class="picaris-group-check" id="picaris-group-ok-<?php echo $group['group_id']; ?>"></span>
                                    <img class="picaris-ico-image" src="<?php echo Mage::getBaseUrl('media') . $group['icon']; ?>" />
                                    <span class="picaris-mobile-arrow-rl">&#9658;</span>
                                    <span class="picaris-group-title"><?php echo $group['label']; ?></span>
                                </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <ul class="picaris-product-page-content-option-group-options">
                        <?php
                        foreach($custdata as $custoption){
                            ?>
                            <li class="picaris-group-item" id="group-item-<?php echo $custoption['group_id']; ?>">
                                <?php if(!empty($custoption['htmltext'])){ ?>
                                    <p><?php echo Mage::helper('cms')->getPageTemplateProcessor()->filter($custoption['htmltext']); ?></p>
                                <?php } ?>
                                <ul class="picaris-attribute-option-list-in-group">
                            <?php
                            if(count($custoption['attributes']) <= 0){
                                ?>
                                    <script type="text/javascript">
                                        alert('Error Occured. Please set attribute members at group <?php echo $custoption['label']; ?>(<?php  echo $custoption['title']; ?>)');
                                    </script>
                                <?php
                            }
                            foreach($custoption['attributes'] as $attribute){
                                ?>
                                    <li id="li-<?php echo $attribute['attribute_id']; ?>">
                                        <?php echo Mage::helper('picaris')->readAttributeJSFile($attribute['attribute_id']); ?>                                        
                                        <?php if(count($attribute['dependencies']) > 0){ ?>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function(){
                                                    <?php
                                                    foreach($attribute['dependencies'] as $dependency){
                                                        foreach($dependency['config']['depender'] as  $depender){
                                                            ?>   
                                                                <?php if(intval($dependency['visible_dependers']) === 0){ ?>    
                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                <?php } ?>
                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disable');
                                                            <?php
                                                        }
                                                       //dropdown with option dependency 
                                                       if($dependency['config']['dependee']['type']==='dropdown' && isset($dependency['config']['dependeeOptionDependency'])){
                                                           ?>
                                                                jQuery('#<?php echo $dependency['dependee'] ?>').change(function(){
                                                                    var dependeeOptions = <?php echo json_encode($dependency['config']['dependeeOptionDependency']); ?>;
                                                                    if(dependeeOptions.indexOf(jQuery(this).val()) > -1){
                                                                        <?php
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                        <?php
                                                                        //rollback to defaults
                                                                        //1902mp
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    
                                                                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?> 
                                                                                        jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                    <?php } ?>
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disable');

                                                                                    switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                                        case 'text':
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                                            break;
                                                                                        case 'checkbox':
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                                                }
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                                            break;
                                                                                        default:
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                                                    if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                                                    }
                                                                                                    else{
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                                                    }
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                                                    jQuery('#<?php echo $dependerj[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.picaris-default-image').trigger('click');
                                                                                                }
                                                                                            break;
                                                                                    }
                                                                                }
                                                                                
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                });
                                                           <?php
                                                       }
                                                       
                                                       //dropdown without option dependency
                                                       elseif($dependency['config']['dependee']['type']==='dropdown' && !isset($dependency['config']['dependeeOptionDependency'])){
                                                           ?>
                                                                jQuery('#<?php echo $dependency['dependee'] ?>').change(function(){
                                                                    if(jQuery(this).val().length > 0){
                                                                       <?php
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                        <?php
                                                                        //rollback to defaults
                                                                        //1902mp
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?> 
                                                                                        jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                    <?php } ?>
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                                                    switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                                        case 'text':
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                                            break;
                                                                                        case 'checkbox':
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                                                }
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                                            break;
                                                                                        default:
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                                                    if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                                                    }
                                                                                                    else{
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                                                    }
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.picaris-default-image').trigger('click');
                                                                                                }
                                                                                            break;
                                                                                    }
                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                });   
                                                           <?php
                                                       }
                                                       //image with option dependency 
                                                       if($dependency['config']['dependee']['type']==='image' && isset($dependency['config']['dependeeOptionDependency'])){
                                                           ?>
                                                                jQuery('.picaris-image-input-<?php echo $dependency['dependee'] ?>').click(function(){
                                                                    var dependeeOptions = <?php echo json_encode($dependency['config']['dependeeOptionDependency']); ?>;
                                                                    if(dependeeOptions.indexOf(jQuery(this).attr('data-optionid')) > -1){
                                                                        <?php
                                                                        foreach($dependency['config']['depender'] as $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');

                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                       <?php
                                                                        //rollback to defaults
                                                                        //1902mp
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                                                        jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                    <?php } ?>
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disable');

                                                                                    switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                                        case 'text':
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                                            break;
                                                                                        case 'checkbox':
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                                                }
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                                            break;
                                                                                        default:
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                                                    if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                                                    }
                                                                                                    else{
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                                                    }
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.picaris-default-image').trigger('click');
                                                                                                    //1902kelvin
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-price-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                                }
                                                                                            break;
                                                                                    }
                                                                                }
                                                                                
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                });
                                                           <?php 
                                                       }
                                                       //image without option dependency
                                                       elseif($dependency['config']['dependee']['type']==='image' && !isset($dependency['config']['dependeeOptionDependency'])){
                                                           ?>
                                                                jQuery('.picaris-image-input-<?php echo $dependency['dependee'] ?>').click(function(){
                                                                    if(jQuery(this).attr('data-optionid').length > 0){
                                                                       <?php
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');

                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                        <?php
                                                                        //rollback to defaults
                                                                        //1902mp
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                                                        jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                    <?php } ?>
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                                                    switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                                        case 'text':
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                                            break;
                                                                                        case 'checkbox':
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                                                }
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                                            break;
                                                                                        default:
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                                                    if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                                                    }
                                                                                                    else{
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                                                    }
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.picaris-default-image').trigger('click');
                                                                                                    //1902kelvin-recopy
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-price-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                                }
                                                                                            break;
                                                                                    }
                                                                                }
                                                                                
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                });   
                                                           <?php
                                                       }
                                                        //checkbox
                                                       elseif($dependency['config']['dependee']['type']==='checkbox'){
                                                           ?>
                                                                jQuery('#<?php echo $dependency['dependee'] ?>').click(function(){
                                                                    if(jQuery('#<?php echo $dependency['dependee'] ?>:checked').length === 1){
                                                                        <?php
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                        <?php
                                                                        //rollback to defaults
                                                                        //1902mp
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                                                        jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                    <?php } ?>
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                                                    switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                                        case 'text':
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                                            break;
                                                                                        case 'checkbox':
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                                                }
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                                            break;
                                                                                        default:
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                                                    if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                                                    }
                                                                                                    else{
                                                                                                        jQuery('#<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                                                    }
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.picaris-default-image').trigger('click');
                                                                                                }
                                                                                            break;
                                                                                    }
                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                });   
                                                           <?php
                                                       }
                                                       //text
                                                       elseif($dependency['config']['dependee']['type']==='text'){
                                                           ?>
                                                                jQuery('#<?php echo $dependency['dependee'] ?>').keyup(function(){
                                                                    if(jQuery(this).val().length > 0){
                                                                        <?php
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                                                }
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                        <?php
                                                                        //rollback to defaults
                                                                        //1902mp
                                                                        foreach($dependency['config']['depender'] as  $depender){
                                                                            ?>
                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').length){
                                                                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                                                        jQuery('#li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                                    <?php } ?>
                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                                                    switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                                        case 'text':
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                                            break;
                                                                                        case 'checkbox':
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                                                }
                                                                                                jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                                            break;
                                                                                        default:
                                                                                                if(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                                                }
                                                                                                else{
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?> .picaris-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                                                    jQuery('#<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                                                    jQuery('#imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.picaris-default-image').trigger('click');
                                                                                                }
                                                                                            break;
                                                                                    }
                                                                                }
                                                                                
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    }
                                                                });   
                                                           <?php
                                                       }

                                                    }
                                                    ?>

                                                });
                                            </script>
                                        <?php
                                        } 
                                        ?>
                                        
                                        <label for="<?php echo $attribute['attribute_id']; ?>">
                                            <script type="text/javascript">
                                            jQuery(document).ready(function(){
                                                picarisPriorityClickFocus('label[for=<?php echo $attribute['attribute_id']; ?>]');
                                            });
                                            </script>
                                            <?php 
                                            $attrib_label = explode('|',$attribute['label']);
                                            if(isset($attrib_label[4]) && !empty($attrib_label[4])){
                                                ?>
                                                <p style="padding:0px; margin:0px;" class="picaris-beforelabel">
                                                <?php
                                                echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[4]);
                                                ?>
                                                </p>
                                                <?php
                                            }
                                            ?>
                                            <h3 class="picaris-thislabel">
                                                <?php 
                                                echo $attrib_label[0];
                                                ?>
                                            <?php if($attribute['config']['required']=== '1'){ ?>*<?php } ?>
                                            </h3>
                                            <?php
                                           switch($attribute['type']){
                                               case 'dropdown':
                                                   ?>
                                                    <?php 
                                                    if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                        ?>
                                                        <p style="padding:0px; margin:0px;" class="picaris-beforelinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                        ?>
                                                        </p>
                                                        <?php
                                                    }
                                                    ?>
                                                    <select style="max-width:100%;" id="<?php echo $attribute['attribute_id']; ?>" name="<?php echo $attribute['attribute_id']; ?>" class="<?php if($attribute['config']['required']=== '1'){ ?>required-entry<?php } ?>">
                                                        <?php
                                                        foreach($attribute['options'] as $option){
                                                            ?>
                                                            <option value="<?php echo $option['option_id']; ?>" 
                                                                    <?php
                                                                    foreach($option['config'] as $configkey => $configval){
                                                                        if(is_array($configval)){
                                                                            ?>
                                                                                data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                            <?php
                                                                        }
                                                                        else{
                                                                            ?>
                                                                                data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>  data-threeddata="<?php echo $option['threed_data']; ?>" <?php if($attribute['config']['default']===$option['option_id']){ ?> selected="selected" <?php } ?>>
                                                                <?php echo $option['config']['label']; ?> 
                                                                <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                         && (floatval($option['config']['price']) > 0.00 
                                                                            || floatval($attribute['config']['price']['amount']) > 0.00)){ ?>
                                                                    +<?php
                                                                    switch($attribute['config']['price']['behavior']){
                                                                        case 'd':
                                                                                echo strval(floatval($option['config']['price']) + floatval($attribute['config']['price']['amount']));
                                                                            break;
                                                                        case 's':
                                                                                if(floatval($option['config']['price']) > 0.00 ){
                                                                                    echo strval(floatval($option['config']['price']));  
                                                                                }
                                                                                else{
                                                                                    echo strval(floatval($attribute['config']['price']['amount']));  
                                                                                }
                                                                            break;
                                                                    }
                                                                    echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
                                                                    ?>
                                                                <?php } ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php 
                                                    if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                        ?>
                                                        <span class="picaris-behindinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                        ?>
                                                        </span>
                                                        <?php
                                                    }
                                                    if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                        ?>
                                                        <p style="padding:0px; margin:0px;" class="picaris-afterinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                        ?>
                                                        </p>
                                                        <?php
                                                    }
                                                    ?>
                                                    <script type="text/javascript">
                                                       jQuery(document).ready(function(){
                                                                picarisprodattr.cmds['<?php echo $attribute['attribute_id']; ?>'] = function(){
                                                                    
                                                                    <?php if(Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3d')){ 
                                                                        $threedCmds = explode(';',$attribute['threed_param']);
                                                                        $threedCmdsOn = array();
                                                                        $threedCmdsOff = array();
                                                                        if(!empty($threedCmds[0])){
                                                                            $threedCmdsOn = explode(',',$threedCmds[0]);
                                                                        }
                                                                        if(!empty($threedCmds[1])){
                                                                            $threedCmdsOff = explode(',',$threedCmds[1]);
                                                                        }
                                                                        ?> 

                                                                        if(jQuery('#<?php echo $attribute['attribute_id']; ?> option:selected').attr('data-threeddata') !== ''){
                                                                            <?php 
                                                                            foreach($threedCmdsOn as $cmdOn){
                                                                                if(!empty($cmdOn)){
                                                                                ?>
                                                                                try{
                                                                                    var threeddata_arr = jQuery('#<?php echo $attribute['attribute_id']; ?> option:selected').attr('data-threeddata').split(',');
                                                                                    for(var i = 0; i < threeddata_arr.length; i++){
                                                                                         sendUnityMessage(procParametized3DData(sprintf("<?php echo $cmdOn; ?>",threeddata_arr[i])),'<?php echo $attribute['attribute_id']; ?>');
                                                                                    }
                                                                                }
                                                                                catch(e){
                                                                                    console.log('Unity 3D not yet ready');
                                                                                }
                                                                                <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        }
                                                                        else{
                                                                            <?php 
                                                                            foreach($threedCmdsOff as $cmdOff){
                                                                                if(!empty($cmdOff)){
                                                                                ?>
                                                                                try{
                                                                                    var threeddata_arr = jQuery('#<?php echo $attribute['attribute_id']; ?> option:selected').attr('data-threeddata').split(',');
                                                                                    for(var i = 0; i < threeddata_arr.length; i++){
                                                                                        sendUnityMessage(procParametized3DData(sprintf("<?php echo $cmdOff; ?>",threeddata_arr[i])),'<?php echo $attribute['attribute_id']; ?>');
                                                                                    }
                                                                                    
                                                                                }
                                                                                catch(e){
                                                                                    console.log('Unity 3D not yet ready');
                                                                                }
                                                                                <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        }
                                                                        
                                                                        //camera view
                                                                        <?php if(isset($attribute['config']['camera'])){ ?>
                                                                            if(!picarisprodattr.lockcamera){
                                                                                try{
                                                                                    var unityMsg = "<msg type='SetCamera' locked='false' x='<?php echo $attribute['config']['camera']['x']; ?>' y='<?php echo $attribute['config']['camera']['y']; ?>' zoom='<?php echo $attribute['config']['camera']['zoom']; ?>' xPos='<?php echo $attribute['config']['camera']['xPos']; ?>' yPos='<?php echo $attribute['config']['camera']['yPos']; ?>' />";
                                                                                    sendUnityMessage(unityMsg);
                                                                                }
                                                                                catch(e){
                                                                                    console.log('Unity 3D not yet ready');
                                                                                }
                                                                            }
                                                                        <?php } ?>
                                                                    
                                                                    <?php } ?>
                                                                };
                                                              
                                                                jQuery('#<?php echo $attribute['attribute_id']; ?>').change(picarisprodattr.cmds['<?php echo $attribute['attribute_id']; ?>']);
                                                                jQuery('#<?php echo $attribute['attribute_id']; ?>').change(function(){
                                                                    changeProductPrice();
                                                                    picarisValidateAll();
                                                                    refresh3DView('<?php echo $attribute['attribute_id']; ?>');
                                                                });
                                                            });
                                                    </script>
                                                   <?php
                                                   break;
                                               case 'image':
                                                   ?>
                                                       
                                                        <?php 
                                                          if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                              ?>
                                                              <p style="padding:0px; margin:0px;" class="picaris-beforelinput">
                                                              <?php
                                                              echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                              ?>
                                                              </p>
                                                              <?php
                                                          }
                                                        ?>
                                                        <ul id="imagelist-<?php echo $attribute['attribute_id']; ?>" class="picaris-image-grouper">
                                                        <?php
                                                        foreach($attribute['options'] as $image){
                                                            ?>
                                                            <li>
                                                                <img class="picaris-image-input-<?php echo $attribute['attribute_id']; ?> 
                                                                   <?php if($attribute['config']['default']===$image['option_id']){ ?>picaris-default-image selected-image<? }?>" 
                                                                   style="margin-top:3px; cursor:pointer; <?php if($attribute['config']['default']===$image['option_id']){?> border:solid 2px #5F9EA0; <?php } ?>"  
                                                                   data-threeddata="<?php echo $image['threed_data']; ?>" data-optionid="<?php echo $image['option_id']; ?>"
                                                                   onclick="if(jQuery('#<?php echo $image['attribute_id']; ?>').prop('disabled')) return false /*prevent execution when input element is disable 1902mp*/;
                                                                                                                                               
                                                                            jQuery('#<?php echo $image['attribute_id']; ?>').val(jQuery(this).attr('data-optionid'));
                                                                            jQuery('.picaris-image-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');
                                                                            //jQuery('#imagelist-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');
                                                                            <?php  if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                                        && (floatval($image['config']['price']) > 0.00 
                                                                                        || floatval($image['config']['price']['amount']) > 0.00)){ ?>
                                                                                <?php
                                                                                switch($attribute['config']['price']['behavior']){
                                                                                    case 'd':
                                                                                        ?>
                                                                                        jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php 
                                                                                            echo strval(floatval($image['config']['price']) + floatval($attribute['config']['price']['amount']));
                                                                                        ?>');
                                                                                        <?php
                                                                                        break;
                                                                                    case 's':
                                                                                        if(floatval($image['config']['price']) > 0.00){
                                                                                            ?>
                                                                                                jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($image['config']['price'])); ?>');                                                                   
                                                                                            <?php
                                                                                        }
                                                                                        else{
                                                                                            ?>
                                                                                                jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($attribute['config']['price']['amount'])); ?>'); 
                                                                                            <?php
                                                                                        }
                                                                                        break;
                                                                                }
                                                                                ?>
                                                                                jQuery('#picaris-image-price-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').css('display','block');
                                                                                //jQuery('#imagelist-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');
                                                                                <?php } ?>"
                                                                            
                                                                   width="47" 
                                                                   height="47" src="<?php echo $image['config']['image']; ?>" 
                                                                   alt=" <?php echo $image['config']['label']; ?>" 
                                                                   title="<?php echo $image['config']['label']; ?>"
																   data-error-retries = "0"
                                                                   id="picaris-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>"
                                                                    <?php
                                                                    foreach($image['config'] as $configkey => $configval){
                                                                        if(is_array($configval)){
                                                                            ?>
                                                                                data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                            <?php
                                                                        }
                                                                        else{
                                                                            ?>
                                                                                data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?> 
                                                                    />
                                                                
                                                                <!-- src handling for dynamic image displays -->
                                                                <script type="text/javascript">
                                                                    jQuery(document).ready(function(){
                                                                       jQuery('#picaris-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').error(function(){
																			if(parseInt(jQuery(this).attr('data-error-retries')) <= 2){
																				jQuery('#picaris-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').addClass('dynamic-image-attribute');
																				jQuery('#picaris-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').attr('data-dynimg',procParametized3DData('<?php echo $image['config']['image']; ?>'));
																				jQuery('#picaris-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').attr('src',procParametized3DData('<?php echo $image['config']['image']; ?>'));
																				jQuery(this).attr('data-error-retries',String(parseInt(jQuery(this).attr('data-error-retries')) + 1));
																			}
                                                                       });
                                                                       jQuery('#group-btn-<?php echo $custoption['group_id']; ?>').click(function(){
                                                                            jQuery('#picaris-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').attr('src',procParametized3DData('<?php echo $image['config']['image']; ?>'));
                                                                       });
                                                                    });
                                                                </script>
                                                                <!-- src handling for dynamic image displays -->
                                                                
                                                                <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                         && (floatval($image['config']['price']) > 0.00 
                                                                            || floatval($image['config']['price']['amount']) > 0.00)){ ?>
                                                                <span class="picaris-image-price-<?php echo $attribute['attribute_id']; ?>" 
                                                                      id="picaris-image-price-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>" 
                                                                      <?php if($attribute['config']['default']!==$image['option_id']){ ?> style="display: none;" <? }?>
                                                                      >
                                                                      +<?php echo strval($image['config']['price']).Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?> 
                                                                </span>
                                                                <?php } ?>
                                                                
                                                                <?php if($attribute['config']['default']===$image['option_id'] 
                                                                        && !in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                        && (floatval($image['config']['price']) > 0.00 
                                                                            || floatval($image['config']['price']['amount']) > 0.00)){ ?>
                                                                <script type='text/javascript'>
                                                                    jQuery(document).ready(function(){
                                                                          //jQuery('#imagelist-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');
                                                                          /*jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo $image['config']['price']; ?>');*/
                                                                          <?php
                                                                            switch($attribute['config']['price']['behavior']){
                                                                                case 'd':
                                                                                    ?>
                                                                                    jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php 
                                                                                        echo strval(floatval($image['config']['price']) + floatval($attribute['config']['price']['amount']));
                                                                                    ?>');
                                                                                    <?php
                                                                                    break;
                                                                                case 's':
                                                                                    if(floatval($image['config']['price']) > 0.00){
                                                                                        ?>
                                                                                            jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($image['config']['price'])); ?>');                                                                   
                                                                                        <?php
                                                                                    }
                                                                                    else{
                                                                                        ?>
                                                                                            jQuery('#imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($attribute['config']['price']['amount'])); ?>'); 
                                                                                        <?php
                                                                                    }
                                                                                    break;
                                                                            }
                                                                        ?>
                                                                    });
                                                                </script>
                                                                <? } ?>
                                                            </li> 
                                                            <?php
                                                        }
                                                        ?>
                                                        </ul>
                                                              
                                                        <input data-defaultvalue="<?php echo $attribute['config']['default']; ?>" 
                                                               value="<?php echo $attribute['config']['default']; ?>" 
                                                               type="hidden" id="<?php echo $attribute['attribute_id']; ?>" 
                                                               name="<?php echo $attribute['attribute_id']; ?>" 
                                                               class="<?php if($attribute['config']['required']=== '1'){ ?> required-entry <?php } ?>">
                                                        <?php /*
                                                        <span id="imagelist-price-<?php echo $attribute['attribute_id']; ?>" style="display:none;">
                                                            +<span id="imagelist-price-text-<?php echo $attribute['attribute_id']; ?>"></span>
                                                            <?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>
                                                        </span>
                                                        */ ?>
                                                        <?php 
                                                        if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                            ?>
                                                              <span class="picaris-behindinput">
                                                            <?php
                                                            echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                            ?>
                                                              </span>
                                                            <?php
                                                        }
                                                        if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                            ?>
                                                            <p style="padding:0px; margin:0px;" class="picaris-afterinput">
                                                            <?php
                                                            echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                            ?>
                                                            </p>
                                                            <?php
                                                        }
                                                        ?>
                                                      
                                                         
                                                        <script type="text/javascript">
                                                            jQuery(document).ready(function(){
                                                                
                                                                picarisprodattr.cmds['picaris-image-input-<?php echo $attribute['attribute_id']; ?>'] = function(){
                                                                    
                                                                    if(jQuery('#<?php echo $attribute['attribute_id']; ?>').prop('disabled')) return false; /*prevent execution when input element is disable 1902mp*/
                                                                    
                                                                    if(typeof jQuery(this).attr('class') !== 'undefined'){
                                                                        jQuery(".picaris-image-input-<?php echo $attribute['attribute_id'] ?>").removeClass('selected-image');
                                                                        jQuery(this).addClass('selected-image');
                                                                        jQuery(this).parent().parent().find('img').css('border','none'); 
                                                                        jQuery(this).css('border','solid 2px #5F9EA0');
                                                                    } 
                                                                    
                                                                     var imgObjSelected = jQuery('.selected-image.picaris-image-input-<?php echo $attribute['attribute_id']; ?>');
                                                                     
                                                                <?php  if(Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3d')){ 
                                                                        $threedCmds = explode(';',$attribute['threed_param']);
                                                                        $threedCmdsOn = explode(',',$threedCmds[0]);
                                                                        $threedCmdsOff = explode(',',$threedCmds[1]);
                                                                        ?>
                                                                       if(imgObjSelected.attr('data-threeddata') !== ''){
                                                                            <?php 
                                                                            foreach($threedCmdsOn as $cmdOn){
                                                                                if(!empty($cmdOn)){
                                                                                ?>
                                                                                try{
                                                                                    var threeddata_arr = imgObjSelected.attr('data-threeddata').split(',');
                                                                                    for(var i = 0; i < threeddata_arr.length; i++){
                                                                                        sendUnityMessage(procParametized3DData(sprintf("<?php echo $cmdOn; ?>",threeddata_arr[i])),'<?php echo $attribute['attribute_id']; ?>');
                                                                                    }
                                                                                }
                                                                                catch(e){
                                                                                    console.log('Unity 3D not yet ready');
                                                                                }
                                                                                <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        }
                                                                        else{
                                                                            <?php 
                                                                            foreach($threedCmdsOff as $cmdOff){
                                                                                if(!empty($cmdOff)){
                                                                                ?>
                                                                                try{
                                                                                    var threeddata_arr = imgObjSelected.attr('data-threeddata').split(',');
                                                                                    for(var i = 0; i < threeddata_arr.length; i++){
                                                                                       sendUnityMessage(procParametized3DData(sprintf("<?php echo $cmdOff; ?>",threeddata_arr[i])),'<?php echo $attribute['attribute_id']; ?>');
                                                                                    }
                                                                                }
                                                                                catch(e){
                                                                                    console.log('Unity 3D not yet ready');
                                                                                }
                                                                                <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        }

                                                                        
                                                                        //camera view
                                                                        <?php if(isset($attribute['config']['camera'])){ ?>
                                                                            if(!picarisprodattr.lockcamera){
                                                                                var unityMsg = "<msg type='SetCamera' locked='false' x='<?php echo $attribute['config']['camera']['x']; ?>' y='<?php echo $attribute['config']['camera']['y']; ?>' zoom='<?php echo $attribute['config']['camera']['zoom']; ?>' xPos='<?php echo $attribute['config']['camera']['xPos']; ?>' yPos='<?php echo $attribute['config']['camera']['yPos']; ?>' />";
                                                                                try{
                                                                                    sendUnityMessage(unityMsg);
                                                                                }
                                                                                catch(e){
                                                                                    console.log('Unity 3D not yet ready');
                                                                                }
                                                                            }
                                                                        <?php } ?>
                                                                            
                                                                    <?php } ?>
                                                                };
                                                               
                                                                jQuery('.picaris-image-input-<?php echo $attribute['attribute_id']; ?>').click(picarisprodattr.cmds['picaris-image-input-<?php echo $attribute['attribute_id']; ?>']);
                                                                jQuery('.picaris-image-input-<?php echo $attribute['attribute_id']; ?>').click(function(){
                                                                    changeProductPrice();  
                                                                    picarisValidateAll();
                                                                    refresh3DView('<?php echo $attribute['attribute_id']; ?>');
                                                                });
                                                            });
                                                        </script>
                                                        
                                                   <?php
                                                   break;
                                               case 'checkbox':
                                                   ?>
                                                    <?php 
                                                      if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                          ?>
                                                          <p style="padding:0px; margin:0px;" class="picaris-beforelinput">
                                                          <?php
                                                          echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                          ?>
                                                          </p>
                                                          <?php
                                                      }
                                                    ?>
                                                   <input <?php if($attribute['config']['default']==='1'){ ?> checked="checked" <?php } ?>
                                                          <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                  && floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                          onclick="if(jQuery(this).prop('checked')){                                                          
                                                                       jQuery('#checkbox-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');                                                         
                                                                        <?php
                                                                       switch($attribute['config']['price']['behavior']){
                                                                           case 'd':
                                                                               /*note yet implemented*/
                                                                               break;
                                                                           case 's':
                                                                               ?>
                                                                               jQuery('#checkbox-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo $attribute['config']['price']['amount']; ?>');
                                                                               <?php
                                                                               break;
                                                                       }
                                                                       ?>
                                                                    }else{
                                                                       jQuery('#checkbox-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');                                                               
                                                                       jQuery('#checkbox-price-text-<?php echo $attribute['attribute_id']; ?>').html('');
                                                                   }"
                                                          <?php } ?>
                                                          name="<?php echo $attribute['attribute_id']; ?>" 
                                                          id="<?php echo $attribute['attribute_id']; ?>" 
                                                          class="<?php if($attribute['config']['required']=== '1'){ ?> required-entry <?php } ?>" 
                                                          type="checkbox"
                                                          <?php
                                                            foreach($attribute['config'] as $configkey => $configval){
                                                                if(is_array($configval)){
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                    <?php
                                                                }
                                                                else{
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                    <?php
                                                                }
                                                            }
                                                            ?> 
                                                          />
                                                        <?php 
                                                        if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                             ?>
                                                              <span class="picaris-behindinput">
                                                            <?php
                                                            echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                            ?>
                                                              </span>
                                                            <?php
                                                        }
                                                        if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                            ?>
                                                            <p style="padding:0px; margin:0px;" class="picaris-afterinput">
                                                            <?php
                                                            echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                            ?>
                                                            </p>
                                                            <?php
                                                        }
                                                        ?>
                                                        
                                                        <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                && floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                        <span id="checkbox-price-<?php echo $attribute['attribute_id']; ?>" <?php if($attribute['config']['default']!== '1'){ ?>style="display: none;"<?php } ?>>
                                                            +<span id="checkbox-price-text-<?php echo $attribute['attribute_id']; ?>">
                                                                <?php echo $attribute['config']['price']['amount']; ?>
                                                            </span><?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>
                                                        </span>
                                                        <?php } ?>    
                                                        
                                                   
                                                       <script type="text/javascript">
                                                       jQuery(document).ready(function(){
                                                           
                                                           picarisprodattr.cmds['<?php echo $attribute['attribute_id']; ?>'] = function(){
                                                               
                                                                <?php if(Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3d')){
                                                                    $threedCmds = explode(';',$attribute['threed_param']);
                                                                    $threedCmdsOn = explode(',',$threedCmds[0]);
                                                                    $threedCmdsOff = explode(',',$threedCmds[1]);
                                                                ?>
                                                                    if(jQuery('#<?php echo $attribute['attribute_id']; ?>').prop('checked')){
                                                                        <?php 
                                                                        foreach($threedCmdsOn as $cmdOn){
                                                                            if(!empty($cmdOn)){
                                                                            ?>
                                                                             try{
                                                                                sendUnityMessage(procParametized3DData(sprintf("<?php echo $cmdOn; ?>")),'<?php echo $attribute['attribute_id']; ?>');
                                                                             }
                                                                             catch(e){
                                                                                console.log('Unity 3D not yet ready');
                                                                             }
                                                                            <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    }
                                                                    else{
                                                                        <?php 
                                                                        foreach($threedCmdsOff as $cmdOff){
                                                                            if(!empty($cmdOff)){
                                                                            ?>
                                                                            try{                                                                
                                                                                sendUnityMessage(procParametized3DData(sprintf("<?php echo $cmdOff; ?>")),'<?php echo $attribute['attribute_id']; ?>');
                                                                            }
                                                                            catch(e){
                                                                                console.log('Unity 3D not yet ready');
                                                                            }
                                                                            <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    }
                                                                       
                                                                <?php } ?>      
                                                                
                                                                //camera view
                                                                <?php if(isset($attribute['config']['camera'])){ ?>
                                                                if(!picarisprodattr.lockcamera){
                                                                    var unityMsg = "<msg type='SetCamera' locked='false' x='<?php echo $attribute['config']['camera']['x']; ?>' y='<?php echo $attribute['config']['camera']['y']; ?>' zoom='<?php echo $attribute['config']['camera']['zoom']; ?>' xPos='<?php echo $attribute['config']['camera']['xPos']; ?>' yPos='<?php echo $attribute['config']['camera']['yPos']; ?>' />";
                                                                    try{
                                                                        sendUnityMessage(unityMsg);
                                                                    }
                                                                    catch(e){
                                                                        console.log('Unity 3D not yet ready');
                                                                    }
                                                                }
                                                                <?php } ?>         
                                                           };

                                                           jQuery('#<?php echo $attribute['attribute_id']; ?>').click(picarisprodattr.cmds['<?php echo $attribute['attribute_id']; ?>']);
                                                           jQuery('#<?php echo $attribute['attribute_id']; ?>').click(function(){
                                                               changeProductPrice();
                                                               picarisValidateAll();
                                                               //refresh3DView();
                                                           });
                                                       });
                                                   </script>
                                                       
                                                   <?php
                                                   break;
                                               case 'text':
                                                   ?>
                                                   <?php 
                                                      if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                          ?>
                                                          <p style="padding:0px; margin:0px;" class="picaris-beforelinput">
                                                          <?php
                                                          echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                          ?>
                                                          </p>
                                                          <?php
                                                      }
                                                    ?>
                                                   <input <?php if(isset($attribute['config']['camera'])){ ?>data-threedcameradata ="<?php echo $attribute['config']['camera']; ?>" <?php } ?>
                                                          maxlength="<?php echo $attribute['config']['maxmin']['max']; ?>"
                                                          value="<?php echo $attribute['config']['default']; ?>"
                                                          <?php if(!in_array($attribute['attribute_id'],$ignoredPrices)
                                                                    && floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                          onkeyup="if(jQuery(this).val().length > 0){
                                                                       jQuery('#text-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');
                                                                       <?php
                                                                       switch($attribute['config']['price']['behavior']){
                                                                           case 'd':
                                                                               ?>
                                                                                jQuery('#text-price-text-<?php echo $attribute['attribute_id']; ?>').html(String(jQuery('#<?php echo $attribute['attribute_id']; ?>').val().length * parseFloat(<?php echo $attribute['config']['price']['amount']; ?>)));
                                                                               <?php
                                                                               break;
                                                                           case 's':
                                                                               ?>
                                                                                jQuery('#text-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo $attribute['config']['price']['amount']; ?>');
                                                                               <?php
                                                                               break;
                                                                       }
                                                                       ?>
                                                                   }else{
                                                                       jQuery('#text-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');                                                               
                                                                       jQuery('#text-price-text-<?php echo $attribute['attribute_id']; ?>').html('');
                                                                   }"
                                                          <?php } ?>
                                                          name="<?php echo $attribute['attribute_id']; ?>"
                                                          type="text"
                                                          class="<?php if($attribute['config']['required']=== '1'){ ?> required-entry <?php } ?>"
                                                          id="<?php echo $attribute['attribute_id']; ?>"
                                                            <?php
                                                            foreach($attribute['config'] as $configkey => $configval){
                                                                if(is_array($configval)){
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                    <?php
                                                                }
                                                                else{
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                    <?php
                                                                }
                                                            }
                                                            ?> 
                                                          >
                                                    <?php 
                                                    if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                        ?>
                                                        <span class="picaris-behindinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                        ?>
                                                        </span>
                                                        <?php
                                                    }
                                                    
                                                    if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                        ?>
                                                        <p style="padding:0px; margin:0px;" class="picaris-afterinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                        ?>
                                                        </p>
                                                        <?php
                                                    }
                                                    
                                                    if(isset($attribute['config']['maxmin']['max']) && intval($attribute['config']['maxmin']['max']) > 0){
                                                        ?>
                                                        <p style="padding:0px; margin:0px;">
                                                        <?php
                                                        echo Mage::helper('picaris')->__('Maximum length is:').strval($attribute['config']['maxmin']['max']);
                                                        ?>
                                                        </p>
                                                        <?php
                                                    }
                                                    ?>
                                                     
                                                    <?php if(floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                        <span id="text-price-<?php echo $attribute['attribute_id']; ?>" <?php if($attribute['config']['default']!== '1'){ ?>style="display: none;"<?php } ?>>
                                                            +<span id="text-price-text-<?php echo $attribute['attribute_id']; ?>">
                                                                <?php echo $attribute['config']['price']['amount']; ?>
                                                            </span><?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>
                                                        </span>
                                                    <?php } ?>

                                                   <script type="text/javascript">
                                                       jQuery(document).ready(function(){
                                                            picarisprodattr.cmds['<?php echo $attribute['attribute_id']; ?>']  = function(){
                                                               
                                                            <?php  if(Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3d')){ 
                                                                    $threedCmds = explode(';',$attribute['threed_param']);
                                                                    $threedCmdsOn = explode(',',$threedCmds[0]);
                                                                    $threedCmdsOff = explode(',',$threedCmds[1]);    
                                                                ?>
                                                              
                                                                if(jQuery('#<?php echo $attribute['attribute_id']; ?>').val().length){
                                                                    <?php 
                                                                        foreach($threedCmdsOn as $cmdOn){
                                                                            if(!empty($cmdOn)){
                                                                            ?>
                                                                            try{                                                               
                                                                                sendUnityMessage(sprintf(procParametized3DData("<?php echo $cmdOn; ?>"),jQuery('#<?php echo $attribute['attribute_id']; ?>').val()),'<?php echo $attribute['attribute_id']; ?>');
                                                                            }
                                                                            catch(e){
                                                                                console.log('Unity 3D not yet ready');
                                                                            }
                                                                            <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                }
                                                                else{
                                                                    <?php 
                                                                        foreach($threedCmdsOff as $cmdOff){
                                                                            if(!empty($cmdOff)){
                                                                            ?>
                                                                            try{
                                                                                sendUnityMessage(sprintf(procParametized3DData("<?php echo $cmdOff; ?>"),jQuery('#<?php echo $attribute['attribute_id']; ?>').val()),'<?php echo $attribute['attribute_id']; ?>');
                                                                            }
                                                                            catch(e){
                                                                                console.log('Unity 3D not yet ready');
                                                                            }
                                                                            <?php
                                                                            }
                                                                        }
                                                                    ?>
                                                                }
                                                               
                                                                //camera view
                                                                <?php if(isset($attribute['config']['camera'])){ ?>
                                                                    if(!picarisprodattr.lockcamera){
                                                                        var unityMsg = "<msg type='SetCamera' locked='false' x='<?php echo $attribute['config']['camera']['x']; ?>' y='<?php echo $attribute['config']['camera']['y']; ?>' zoom='<?php echo $attribute['config']['camera']['zoom']; ?>' xPos='<?php echo $attribute['config']['camera']['xPos']; ?>' yPos='<?php echo $attribute['config']['camera']['yPos']; ?>' />";
                                                                        try{
                                                                            sendUnityMessage(unityMsg);
                                                                        }
                                                                        catch(e){
                                                                            console.log('Unity 3D not yet ready');
                                                                        }
                                                                    }
                                                                <?php } ?>
                                                                                   
                                                            <?php } ?>
                                                                       
                                                           };
                                                            
                                                           jQuery('#<?php echo $attribute['attribute_id']; ?>').keyup(picarisprodattr.cmds['<?php echo $attribute['attribute_id']; ?>']);
                                                           jQuery('#<?php echo $attribute['attribute_id']; ?>').keyup(function(){
                                                                changeProductPrice();
                                                                picarisValidateAll();
                                                                //refresh3DView();
                                                           });
                                                       });
                                                   </script>
                                                   <?php
                                                   break;
                                               default:
                                                   die('<b>'.$attribute['type'].' is invalid option type</b>');
                                           }
                                            ?>
                                        </label>
                                    </li>
                                <?php
                            }
                            ?>
                                 </ul>
                                <?php
                                if(!isset($lctr)){
                                    $lctr = 0;
                                }
                                $lctr++;
                                if($lctr !== count($custdata)){
                                    ?>
                                        <input type="button" value="<?php echo $this->__('Next'); ?> &gt;" class="picaris-custom-option-next"/>
                                    <?php
                                }
                                ?>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                 
                </div>
            </div>
            <div class="picaris-product-page-content-mid">
                
                <?php
                $usepicaris = Mage::getStoreConfig('picaris/picaris_run_group/useproductimpicaris');
                $useunity3d = Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3d');
                $useunity3dAsDefault = Mage::getStoreConfig('picaris/picaris_run_group/useproductimunity3dasdefault');
                
                if($sproduct->getData('picaris_manage_prodimage')){
                    $useunity3dAsDefault = $sproduct->getData('picaris_useunity3dasdefault');
                    $useunity3d = $sproduct->getData('picaris_useunity3dimages');
                    $usepicaris = $sproduct->getData('picaris_useprodimages');
                }
                
                ?>
                
                <div style="text-align:right; font-size:12px;" id="picaris-product-view">
                    
                    <?php if ($usepicaris){ ?>
                    <a href="#" onclick="jQuery('.picaris-product-image-panel').css('display','none'); jQuery('#product-img-useproductimpicaris').css('display','block');">
                        <?php echo $this->__('Image View'); ?>
                    </a>
                    <?php } ?>
                    
                    <?php
                    if($useunity3d && $usepicaris){
                        ?>
                    |
                        <?php
                    }
                    else{
                        ?>
                    <style type="text/css">
                        #picaris-product-view{
                            display:none;
                        }
                    </style>
                        <?php
                    }
                    ?>
                    
                    <?php if ($useunity3d){ ?>
                    <a href="#" onclick="jQuery('.picaris-product-image-panel').css('display','none'); jQuery('#product-img-useproductimunity3d').css('display','block');">
                        <?php echo $this->__('3D View'); ?>
                    </a>
                    <?php } ?>
                   
                </div>
                
                <?php if ($useunity3d){ ?>
                <div id="product-img-useproductimunity3d" class="picaris-product-image-panel" 
                    <?php 
                    if ($useunity3dAsDefault || !$usepicaris){
                        ?>
                        style="display:block;"
                        <?php
                    }
                    ?>
                    >
                    
                    <h3 id="preload-prompt-no-unityplayer" style="color:#ff0000; position:relative; top:150px; text-align:center; margin:0 auto;">
                        <?php echo Mage::helper('picaris')->__('Unity 3D is required. Please turn on <a target="_blank" class="unity3dlink" href="http://unity3d.com/webplayer">Unity 3D</a> plugin'); ?>
                    </h3>
                    
                    <div id="unitywrapper-overlay" class="unitywrapper-overlay"></div>
                    <div id="unitywrapper-window" class="unitywrapper-window"></div>
                </div>
                <?php } ?>
                <?php if ($usepicaris){ ?>
                <div id="product-img-useproductimpicaris" class="picaris-product-image-panel" 
                    <?php 
                    if (!$useunity3dAsDefault){
                        ?>
                        style="display:block;"
                        <?php
                    }
                    ?>
                >
                    
                    <div class="picaris-product-image-div">
                        <?php /* <img id="picaris-product-image" src="<?php echo $tproduct['picaris_prod_picarisimage_url']; ?>" /> */ ?>
                        <img style="max-width:600px; max-height: 500px;" id="picaris-product-image" src="<?php echo Mage::helper('catalog/image')->init($sproduct, 'image')->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)->resize(979,null); ?>" />
                    </div>
                    
                    <?php /*
                    <div class="picaris-product-image-views-div">
                        <img width="100" height="50" title="<?php echo $this->__('Front'); ?>" id="picaris-product-image-view-front" src="<?php echo $tproduct['picaris_prod_frontview_tb']; ?>" />
                        <img width="100" height="50" title="<?php echo $this->__('Back'); ?>" id="picaris-product-image-view-back" src="<?php echo $tproduct['picaris_prod_backview_tb']; ?>" />
                    </div>
                    */ ?>
                    
                </div>
                <?php } ?>
                
            </div>
            <div class="picaris-product-page-content-right">
                <div id="picaris-product-page-content-right-spacefirst"></div>
                <div id="picaris-product-page-content-right-content">
                    <h1 class='picaris-product-name'>
                        <?php echo $tproduct['name']; ?>
                    </h1>
                    <h2 class='picaris-product-price'>
                        <?php echo Mage::helper('core')->currency($sproduct->getPrice(), true, false); ?>
                    </h2>
                    
                    <?php if(!empty($tproduct['picaris_product_save_amt_text'])){
                        ?>
                        <h4 class='picaris-product-save-amt-text'>
                            <?php echo $tproduct['picaris_product_save_amt_text']; ?>                     
                        </h4>
                        <?php
                    } ?>
                    
                    <?php if(!empty($tproduct['picaris_product_includes_text'])){
                        ?><span class='picaris-product-includes'>
                             <span style="font-weight:bold;"><?php echo $this->__('Includes'); ?>: </span><?php echo $tproduct['picaris_product_includes_text']; ?> 
                        </span>
                        <?php
                    } ?>
                    
                    <?php
                    if($tproduct->getStockItem()->getIsInStock()){
                       ?>
                        <input class='picaris-product-submit' type="submit" value="<?php echo Mage::helper('picaris')->__('Buy'); ?>" style="">
                       <?php
                    }
                    else{
                        ?>
                        <input style="background-color: #d6d6d6;
                                    border: medium none !important;
                                    color: #999699;
                                    cursor: pointer;
                                    font-family: raleway;
                                    font-size: 18px;
                                    margin-top: 50px;
                                    padding: 20px 0;
                                    width: 100%;"
                                    type="button" 
                                    value="<?php echo $this->__('Out of Stock'); ?>" 
                                    />
                        <?php
                    }
                    ?>
                    
                    <?php 
                    if(!$tproduct->getStockItem()->getIsInStock()){
                        ?>
                        <style type="text/css">
                            .picaris-product-more-info-below{
                                margin-top:20px;
                            }            
                        </style>
                        <span style="margin-top:10px;"><?php echo $tproduct->getData('picaris_outofstock_msg');  ?></span>
                        <?php
                    }
                    ?>
                    <span onclick="jQuery('html, body').animate({ scrollTop: jQuery(document).height()}, 50);" class='picaris-product-more-info-below'><?php echo Mage::helper('picaris')->__('More information about the product below'); ?>&nbsp;&darr;</span>
                </div>
               
            </div>
        </div>
        <div class="picaris-product-page-footer">
            <div class="picaris-product-page-footer-left"></div>
            <div class="picaris-product-page-footer-mid">
                <?php if(!empty($tproduct['picaris_product_description'])){
                    ?>
                    <h3 class='picaris-product-description-head'><?php echo Mage::helper('picaris')->__('Description'); ?></h3>
                    <span class='picaris-product-description-content'>
                    <?php echo $tproduct['picaris_product_description']; ?>  
                    </span>
                    <?php
                } ?>
            </div>
            <div class="picaris-product-page-footer-right"></div>
        </div>
    </div>
</form>
<?php echo $this->getChildHtml('smart'); ?>

<?php
$allowcharsspecchars = Mage::getStoreConfig('picaris/picaris_attr_group/picaris_attr_group_specchartoallow');
$allowcharsregex = Mage::getStoreConfig('picaris/picaris_attr_group/picaris_attr_group_regexchartoallow');
$supportaccentchars = Mage::getStoreConfig('picaris/picaris_attr_group/picaris_attr_group_supportchars');

if(!empty($allowcharsspecchars) && !empty($allowcharsregex)){ ?>
<script type="text/javascript">
jQuery(document).ready(function(){
    var specialCharacters = "<?php echo $allowcharsspecchars ?>".replace(/[-[\]{}()*+?.,\\/^$|#\s]/g, "\\$&");
    var supportCharacters = "<?php echo $supportaccentchars ?>".replace(/[-[\]{}()*+?.,\\/^$|#\s]/g, "\\$&");
    
    var regexCharacters = "<?php echo $allowcharsregex; ?>";
    var sregex = "^[" + regexCharacters + specialCharacters + supportCharacters + "]+$";
    var regexCompiled = new RegExp(sregex);
    var prevstatetext= [];
        
    var nregex = "^[" + regexCharacters + specialCharacters + "]+$";
    
    try
    {
        nregex = decodeURIComponent( escape(nregex) );
    } catch(e){}
    
    var nregexCompiled = new RegExp(nregex);
    
    var spregex = "^[" + supportCharacters + "]+$";
    var spregexCompiled = new RegExp(spregex);
    
    jQuery('#webgl-form input[type=text], #picaris-custom-options-form input[type=text], #smart-custom-options-form input[type=text], .product-view input[type=text]').each(function(){
        prevstatetext[jQuery(this).attr('id')] = jQuery(this).val();
    });
        
    jQuery('#webgl-form input[type=text], #picaris-custom-options-form input[type=text], #smart-custom-options-form input[type=text], .product-view input[type=text]').bind('input propertychange paste',function(e){   
        var obj = jQuery(this);
        if(!regexCompiled.test(obj.val())){
            if(prevstatetext[obj.attr('id')] < obj.val()){
                var removed_support_chars = '';
                for(var i = 0; i < obj.val().length; i++){
                    if(nregexCompiled.test(obj.val()[i])){
                        removed_support_chars += obj.val()[i];
                    }
                }
                prevstatetext[obj.attr('id')] = removed_support_chars;
                obj.val(prevstatetext[obj.attr('id')]);
            }
        }
        else{
            var count_support_chars = 0;
            for(var i = 0; i < obj.val().length; i++){
                if(spregexCompiled.test(obj.val()[i])){
                    count_support_chars++;
                }
                if(count_support_chars > 1){

                    var removed_support_chars = '';
                    for(var i = 0; i < obj.val().length; i++){
                        if(nregexCompiled.test(obj.val()[i])){
                            removed_support_chars += obj.val()[i];
                        }
                    }
                    prevstatetext[obj.attr('id')] = removed_support_chars;
                    obj.val(prevstatetext[obj.attr('id')]);

                    break;
                }
            }
        }
        obj.trigger('keyup');
    }).blur(function(){
        var obj = jQuery(this);
        var removed_support_chars = '';
        for(var i = 0; i < obj.val().length; i++){
            if(nregexCompiled.test(obj.val()[i])){
                removed_support_chars += obj.val()[i];
            }
        }
        prevstatetext[obj.attr('id')] = removed_support_chars;
        obj.val(prevstatetext[obj.attr('id')]);
        obj.trigger('keyup');
    });  
    
    
});
</script>
<?php }
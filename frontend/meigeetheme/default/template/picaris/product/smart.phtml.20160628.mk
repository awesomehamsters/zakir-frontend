<?php
$tproduct = Mage::getModel('catalog/product')->load($this->getProduct()->getId());
$custdata = Mage::helper('picaris')->collectProductData($tproduct->getData('entity_id'));
$has3levelGroups = explode(',',$this->getHas3rdlevel());
?>
<script type="text/javascript">
        
    smartexprod.translate = [];
    smartexprod.mage = <?php echo json_encode($tproduct->getData()); ?>;
    smartexprod.translate['Buy'] = '<?php echo Mage::helper('picaris')->__('Buy'); ?>';
    smartexprod.translate['Done'] = '<?php echo Mage::helper('picaris')->__('Done'); ?>';
    smartexprod.translate['Out of stock'] = '<?php echo $this->__('Out of stock'); ?>';
    smartexprod.cmds = [];
    smartexprod.cust =  <?php echo json_encode($custdata); ?>;
    smartexprod.custmap = [];
    smartCustAttributesMap();
    smartIgnorePriceProcess();
    smartexprod.currencySymbol = '<?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>';
    smartexprod.has3rdlevel = <?php echo json_encode($has3levelGroups); ?>;
    
    smartexprod.bigImage = "<?php echo Mage::helper('catalog/image')->init($tproduct, 'image')->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)->resize(1279,null); ?>";
    smartexprod.smallImage = "<?php echo Mage::helper('catalog/image')->init($tproduct, 'small_image')->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)->resize(414,null); ?>";
    smartexprod.getIsInStock = <?php echo $tproduct->getStockItem()->getIsInStock(); ?>;
    
    jQuery(window).resize(function(){
        if(jQuery(window).width() <= 414){
            jQuery('#smart-content-menu-level-1-productimg').attr('src',smartexprod.smallImage);
        }
        else if(jQuery(window).width() > 414){
            jQuery('#smart-content-menu-level-1-productimg').attr('src',smartexprod.bigImage);
        }
    });
    
    /* commented out by 1902mp as creates a undefined error with  $('#smart-header-menubtn-btn-img')
     * the $ sign is overrided by prototype.js by magento please use jQuery instead
     * 20150305
    $('#smart-header-menubtn-btn-img').on('click',function(e){
        var answer=confirm('If you leave this page, you will lose your changes. Do you want to continue?');
        if(answer){
         alert("kjhl");
        }
        else{
         e.preventDefault();      
        }
    });
    */
</script>
<form id="smart-custom-options-form" method="post" action="<?php echo Mage::getBaseUrl(); ?>picaris/index/addtocart">
    <input type="hidden" name="sku" value="<?php echo $tproduct->getData('sku'); ?>" style="display:none;">
    <div class="smart-header">
        <div class="smart-header-menubtn">
            <a href="http://shop.clseifert.com/" class="smart-header-menubtn-btn-link">
                <img src="http://shop.clseifert.com/skin/frontend/meigeetheme/default/images/logo-CLSeifert-black.png" class="smart-header-menubtn-btn smart-header-menubtn-btn-img">
                <!--<span class="smart-header-menubtn-btn">
                   <?php //echo Mage::helper('picaris')->__('Menu'); ?>
                </span>-->
            </a>
        </div>
        <div class="smart-header-title">
            <h1><?php echo $tproduct->getData('name'); ?></h1>
        </div>
        <div class="smart-header-bag">
            <a href="<?php echo Mage::helper('checkout/cart')->getCartUrl(); ?>" id="smart-header-bag-btn-link">
                <span class="smart-header-bag-btn">
                    <?php echo Mage::helper('picaris')->__('Bag'); ?>&nbsp;(<?php echo Mage::helper('checkout/cart')->getCart()->getItemsCount(); ?>)
                </span>
            </a>
        </div>
    </div>
    <div class="smart-content">
        
        <div class="smart-content-menu-level-1">
            <div class="smart-content-menu-level-1-imagediv">
                <img id="smart-content-menu-level-1-productimg" src="<?php echo Mage::helper('catalog/image')->init($tproduct, 'image')->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)->resize(979,null); ?>">   
            </div>
            <div class="smart-content-menu-level-1-optionsdiv">
                <ul class="smart-content-menu-level-1-optionsdiv-groups">
                     <?php if(count($custdata) <= 0){
                        ?>
                        <script type="text/javascript">
                            alert('Error Occured. Please create groups for this product');
                        </script>
                        <?php
                    } ?>
                    <?php
                        $fillInGaps = 3;
                        foreach($custdata as $group){
                            if($fillInGaps === 0) $fillInGaps = 3;
                        ?>
                            <li class="smart-group-btn smart-group-btn-on" data-label="<?php echo $group['label']; ?>" data-isvisited="0" data-threedcamera="<?php echo $group['threed_camera']; ?>" data-group-id="<?php echo $group['group_id']; ?>" id="smart-group-btn-<?php echo $group['group_id']; ?>">
                                <a href="#">
                                    <span style="visibility: hidden;" class="smart-group-btn-check" id="smart-group-ok-<?php echo $group['group_id']; ?>"></span>
                                    <img class="smart-content-menu-level-1-optionsdiv-groups-a-icon" src="<?php echo Mage::getBaseUrl('media') . $group['icon']; ?>" />
                                    <h2>
                                        <?php echo $group['label']; ?>
                                    </h2>
                                </a>
                            </li>
                        <?php
                            $fillInGaps--;
                        }
                        if($fillInGaps > 0 && $fillInGaps <= 2) echo str_repeat('<li class="smart-group-btn" style="margin:0px;"></li>',intval($fillInGaps));
                    ?>
                </ul>
            </div>
        </div>

        <div class="smart-content-menu-level-2ex" style="display:none;">
            
            <?php
            foreach($custdata as $custoption){
            ?>    
            <!-- group load up start -->
                <ul>
                    <li id="smartex-group-<?php echo $custoption['group_id']; ?>" class="smartex-group">
                        <ul class="smartex-group-ul">
                            <?php
                            foreach($custoption['attributes'] as $attribute){
                                ?>
                                <!-- attribute load start -->
                                <li id="smartex-li-<?php echo $attribute['attribute_id']; ?>" class="smartex-attribute" data-group-id="<?php echo $custoption['group_id']; ?>">

                                    <!-- attribute label start -->
                                    <?php $attrib_label = explode('|',$attribute['label']); ?>
                                    <label for="smartex-<?php echo $attribute['attribute_id']; ?>" data-attribute-label="<?php echo $attrib_label[0]; ?>" data-attribute-id="<?php echo $attribute['attribute_id']; ?>" data-group-id="<?php echo $custoption['group_id']; ?>" id="smartex-attribute-label-<?php echo $attribute['attribute_id']; ?>" class="smartex-attribute-label">
                                        
                                        <?php
                                            if(isset($attrib_label[4]) && !empty($attrib_label[4])){
                                                ?>
                                                <p style="padding:0px; margin:0px;" class="smartex-beforelabel">
                                                    <?php
                                                    echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[4]);
                                                    ?>
                                                </p>
                                                <?php
                                            }
                                        ?>
                                        <h3 class="smartex-thislabel">
                                            <?php 
                                                echo $attrib_label[0];
                                            ?>
                                            <?php if($attribute['config']['required']=== '1'){ ?><span style="color:#ff0000;">*</span><?php } ?>
                                            <span class="smartex-attribute-arrow">&#9658;</span>
                                        </h3> 
                                    </label>
                                    <!-- attribute label end -->
                                    
                                    <!-- attribute content start -->
                                    <div id="smartex-attribute-content-<?php echo $attribute['attribute_id']; ?>" class="smartex-attribute-content">
                                    <?php 
                                    switch($attribute['type']){

                                        case 'dropdown':
                                            ?>
                                            <?php if(isset($attrib_label[1]) && !empty($attrib_label[1])){ ?>
                                                <p style="padding:0px; margin:0px;" class="smartex-beforelinput">
                                                <?php
                                                    echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                ?>
                                                </p>
                                            <?php } ?>
                                            <select style="max-width:100%;" id="smartex-<?php echo $attribute['attribute_id']; ?>" name="<?php echo $attribute['attribute_id']; ?>" class="<?php if($attribute['config']['required']=== '1'){ ?>required-entry<?php } ?> smartex-select">
                                                <?php
                                                foreach($attribute['options'] as $option){
                                                    ?>
                                                    <option value="<?php echo $option['option_id']; ?>" 
                                                            <?php
                                                            foreach($option['config'] as $configkey => $configval){
                                                                if(is_array($configval)){
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                    <?php
                                                                }
                                                                else{
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                    <?php
                                                                }
                                                            }
                                                            ?>  data-threeddata="<?php echo $option['threed_data']; ?>" <?php if($attribute['config']['default']===$option['option_id']){ ?> selected="selected" <?php } ?>>
                                                        <?php echo $option['config']['label']; ?> 
                                                        <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                 && (floatval($option['config']['price']) > 0.00 
                                                                    || floatval($attribute['config']['price']['amount']) > 0.00)){ ?>
                                                            +<?php
                                                            switch($attribute['config']['price']['behavior']){
                                                                case 'd':
                                                                        echo strval(floatval($option['config']['price']) + floatval($attribute['config']['price']['amount']));
                                                                    break;
                                                                case 's':
                                                                        if(floatval($option['config']['price']) > 0.00 ){
                                                                            echo strval(floatval($option['config']['price']));  
                                                                        }
                                                                        else{
                                                                            echo strval(floatval($attribute['config']['price']['amount']));  
                                                                        }
                                                                    break;
                                                            }
                                                            echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
                                                            ?>
                                                        <?php } ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                                
                                            <?php 
                                            if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                ?>
                                                <span class="smartex-behindinput">
                                                    <?php
                                                    echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                    ?>
                                                </span>
                                                <?php
                                            }
                                            if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                ?>
                                                <p style="padding:0px; margin:0px;" class="smartex-afterinput">
                                                    <?php
                                                    echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                    ?>
                                                </p>
                                                <?php
                                            }
                                            ?>

                                            <script type="text/javascript">
                                               jQuery(document).ready(function(){
                                                    smartexprod.cmds['<?php echo $attribute['attribute_id']; ?>'] = function(){
                                                        //Extra event handle
                                                    };

                                                    jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').change(smartexprod.cmds['<?php echo $attribute['attribute_id']; ?>']);
                                                    jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').change(function(){
                                                        smartChangeProductPrice();
                                                        smartValidateAll();
                                                    });
                                                });
                                            </script>
                                                
                                            <?php
                                            break;

                                        case 'image':
                                            ?>
                                               

                                                <?php 
                                                  if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                      ?>
                                                      <p style="padding:0px; margin:0px;" class="smartex-beforelinput">
                                                      <?php
                                                      echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                      ?>
                                                      </p>
                                                      <?php
                                                  }
                                                ?>

                                                <ul id="smartex-imagelist-<?php echo $attribute['attribute_id']; ?>" class="smartex-image-grouper">
                                                <?php
                                                foreach($attribute['options'] as $image){
                                                    ?>
                                                    <li>
                                                        <img class="smartex-image-input-<?php echo $attribute['attribute_id']; ?> 
                                                           <?php if($attribute['config']['default']===$image['option_id']){ ?>smartex-default-image selected-image<? }?>" 
                                                           style="margin-top:3px; cursor:pointer; <?php if($attribute['config']['default']===$image['option_id']){?> border:solid 2px #5F9EA0; <?php } ?>"  
                                                           data-threeddata="<?php echo $image['threed_data']; ?>" 
                                                           data-optionid="<?php echo $image['option_id']; ?>" 
                                                           onclick="if(jQuery('#smartex-<?php echo $image['attribute_id']; ?>').prop('disabled')) return false /*prevent execution when input element is disable 1902mp*/;
                                                                    jQuery('#smartex-<?php echo $image['attribute_id']; ?>').val(jQuery(this).attr('data-optionid'));
                                                                    jQuery('.smartex-image-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');
                                                                    //jQuery('#smartex-imagelist-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');
                                                                    <?php  if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                                && (floatval($image['config']['price']) > 0.00 
                                                                                    || floatval($image['config']['price']['amount']) > 0.00)){ ?>
                                                                        <?php
                                                                        switch($attribute['config']['price']['behavior']){
                                                                            case 'd':
                                                                                ?>
                                                                                jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php 
                                                                                    echo strval(floatval($image['config']['price']) + floatval($attribute['config']['price']['amount']));
                                                                                ?>');
                                                                                <?php
                                                                                break;
                                                                            case 's':
                                                                                if(floatval($image['config']['price']) > 0.00){
                                                                                    ?>
                                                                                        jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($image['config']['price'])); ?>');                                                                   
                                                                                    <?php
                                                                                }
                                                                                else{
                                                                                    ?>
                                                                                        jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($attribute['config']['price']['amount'])); ?>'); 
                                                                                    <?php
                                                                                }
                                                                                break;
                                                                        }
                                                                        ?>

                                                                        jQuery('#smartex-image-price-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').css('display','block');
                                                                        //jQuery('#smartex-imagelist-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');
                                                                    <?php } ?>"
                                                           width="47" 
                                                           height="47" src="<?php echo $image['config']['image']; ?>" 
                                                           alt=" <?php echo $image['config']['label']; ?>" 
                                                           title="<?php echo $image['config']['label']; ?>"
														   data-error-retries = "0"
                                                           id="smartex-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>"
                                                            <?php
                                                            foreach($image['config'] as $configkey => $configval){
                                                                if(is_array($configval)){
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                    <?php
                                                                }
                                                                else{
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                    <?php
                                                                }
                                                            }
                                                            ?> 
                                                            />
                                                        
                                                        <!-- src handling for dynamic image displays -->
                                                            <script type="text/javascript">
                                                                jQuery(document).ready(function(){
                                                                   jQuery('#smartex-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').error(function(){
																		if(parseInt(jQuery(this).attr('data-error-retries')) <= 2){
																			jQuery('#smartex-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').attr('src',procParametized3DData('<?php echo $image['config']['image']; ?>'));
																			jQuery(this).attr('data-error-retries',String(parseInt(jQuery(this).attr('data-error-retries')) + 1));
																		}
																   });
                                                                   jQuery('#smartex-group-btn-<?php echo $custoption['group_id']; ?>').click(function(){
                                                                        jQuery('#smartex-image-input-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>').attr('src',procParametized3DData('<?php echo $image['config']['image']; ?>'));
                                                                   });
                                                                });
                                                            </script>
                                                        <!-- src handling for dynamic image displays -->
                                                        
                                                        <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                 && (floatval($image['config']['price']) > 0.00 
                                                                    || floatval($image['config']['price']['amount']) > 0.00)){ ?>
                                                        <span class="smartex-image-price-<?php echo $attribute['attribute_id']; ?>" 
                                                              id="smartex-image-price-<?php echo $attribute['attribute_id']; ?>-<?php echo $image['option_id']; ?>" 
                                                              <?php if($attribute['config']['default']!==$image['option_id']){ ?> style="display: none;" <? }?>
                                                              >
                                                              +<?php echo strval($image['config']['price']).Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?> 
                                                        </span>
                                                        <?php } ?>

                                                        <?php if($attribute['config']['default']===$image['option_id'] 
                                                                && !in_array($attribute['attribute_id'],$ignoredPrices) 
                                                                && (floatval($image['config']['price']) > 0.00 
                                                                    || floatval($image['config']['price']['amount']) > 0.00)){ ?>
                                                        <script type='text/javascript'>
                                                            jQuery(document).ready(function(){
                                                                  //jQuery('#smartex-imagelist-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');
                                                                  /*jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo $image['config']['price']; ?>');*/
                                                                  <?php
                                                                    switch($attribute['config']['price']['behavior']){
                                                                        case 'd':
                                                                            ?>
                                                                            jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php 
                                                                                echo strval(floatval($image['config']['price']) + floatval($attribute['config']['price']['amount']));
                                                                            ?>');
                                                                            <?php
                                                                            break;
                                                                        case 's':
                                                                            if(floatval($image['config']['price']) > 0.00){
                                                                                ?>
                                                                                    jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($image['config']['price'])); ?>');                                                                   
                                                                                <?php
                                                                            }
                                                                            else{
                                                                                ?>
                                                                                    jQuery('#smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo strval(floatval($attribute['config']['price']['amount'])); ?>'); 
                                                                                <?php
                                                                            }
                                                                            break;
                                                                    }
                                                                ?>
                                                            });
                                                        </script>
                                                        <? } ?>
                                                    </li> 
                                                    <?php
                                                }
                                                ?>    
                                                </ul>
                                                      
                                                <input data-defaultvalue="<?php echo $attribute['config']['default']; ?>" 
                                                       value="<?php echo $attribute['config']['default']; ?>" 
                                                       type="hidden" 
                                                       id="smartex-<?php echo $attribute['attribute_id']; ?>" 
                                                       name="<?php echo $attribute['attribute_id']; ?>" 
                                                       class="<?php if($attribute['config']['required']=== '1'){ ?> required-entry <?php } ?>">
                                                <?php /*
                                                <span id="smartex-imagelist-price-<?php echo $attribute['attribute_id']; ?>" style="display:none;">
                                                    +<span id="smartex-imagelist-price-text-<?php echo $attribute['attribute_id']; ?>"></span>
                                                    <?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>
                                                </span>
                                                 * 
                                                 */ ?>

                                                <?php 
                                                if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                    ?>
                                                    <span class="smartex-behindinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                        ?>
                                                    </span>
                                                    <?php
                                                }
                                                if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                    ?>
                                                    <p style="padding:0px; margin:0px;" class="smartex-afterinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                        ?>
                                                    </p>
                                                    <?php
                                                }
                                                ?>

                                                <script type="text/javascript">
                                                    jQuery(document).ready(function(){

                                                        smartexprod.cmds['smartex-image-input-<?php echo $attribute['attribute_id']; ?>'] = function(){
                                                            if(jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').prop('disabled')) return false; /*prevent execution when input element is disable 1902mp*/
                                                             
                                                            if(typeof jQuery(this).attr('class') !== 'undefined'){
                                                                jQuery(".smartex-image-input-<?php echo $attribute['attribute_id'] ?>").removeClass('selected-image');
                                                                jQuery(this).addClass('selected-image');
                                                                jQuery(this).parent().parent().find('img').css('border','none'); 
                                                                jQuery(this).css('border','solid 2px #5F9EA0');
                                                            } 

                                                            var imgObjSelected = jQuery('.selected-image.smartex-image-input-<?php echo $attribute['attribute_id']; ?>');
                                                        };

                                                        jQuery('.smartex-image-input-<?php echo $attribute['attribute_id']; ?>').click(smartexprod.cmds['smartex-image-input-<?php echo $attribute['attribute_id']; ?>']);
                                                        jQuery('.smartex-image-input-<?php echo $attribute['attribute_id']; ?>').click(function(){
                                                            smartChangeProductPrice();  
                                                            smartValidateAll();
                                                        });
                                                    });
                                                </script>

                                           <?php
                                            break;

                                            case 'checkbox':
                                                ?>

                                                <?php 
                                                  if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                      ?>
                                                      <p style="padding:0px; margin:0px;" class="smartex-beforelinput">
                                                      <?php
                                                      echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                      ?>
                                                      </p>
                                                      <?php
                                                  }
                                                ?>

                                                <input  <?php if($attribute['config']['default']==='1'){ ?> checked="checked" <?php } ?>
                                                        <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                              && floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                        onclick="if(jQuery(this).is(':checked')){                                                          
                                                                   jQuery('#smartex-checkbox-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');                                                         
                                                                    <?php
                                                                   switch($attribute['config']['price']['behavior']){
                                                                       case 'd':
                                                                           /*note yet implemented*/
                                                                           break;
                                                                       case 's':
                                                                           ?>
                                                                           jQuery('#smartex-checkbox-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo $attribute['config']['price']['amount']; ?>');
                                                                           <?php
                                                                           break;
                                                                   }
                                                                   ?>
                                                                }else{
                                                                   jQuery('#smartex-checkbox-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');                                                               
                                                                   jQuery('#smartex-checkbox-price-text-<?php echo $attribute['attribute_id']; ?>').html('');
                                                               }"
                                                      <?php } ?>
                                                      name="<?php echo $attribute['attribute_id']; ?>" 
                                                      id="smartex-<?php echo $attribute['attribute_id']; ?>" 
                                                      class="<?php if($attribute['config']['required']=== '1'){ ?> required-entry <?php } ?> smartex-checkbox" 
                                                      type="checkbox"
                                                      <?php
                                                        foreach($attribute['config'] as $configkey => $configval){
                                                            if(is_array($configval)){
                                                                ?>
                                                                    data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                <?php
                                                            }
                                                            else{
                                                                ?>
                                                                    data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                <?php
                                                            }
                                                        }
                                                        ?> 
                                                />

                                                <?php 
                                                    if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                         ?>
                                                          <span class="smartex-behindinput">
                                                            <?php
                                                            echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                            ?>
                                                          </span>
                                                        <?php
                                                    }
                                                    if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                        ?>
                                                        <p style="padding:0px; margin:0px;" class="smartex-afterinput">
                                                            <?php
                                                            echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                            ?>
                                                        </p>
                                                        <?php
                                                    }
                                                ?>

                                                <?php if(!in_array($attribute['attribute_id'],$ignoredPrices) 
                                                            && floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                <span id="smartex-checkbox-price-<?php echo $attribute['attribute_id']; ?>" <?php if($attribute['config']['default']!== '1'){ ?>style="display: none;"<?php } ?>>
                                                    +<span id="smartex-checkbox-price-text-<?php echo $attribute['attribute_id']; ?>">
                                                        <?php echo $attribute['config']['price']['amount']; ?>
                                                    </span><?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>
                                                </span>
                                                <?php } ?>

                                                <script type="text/javascript">
                                                   jQuery(document).ready(function(){

                                                       smartexprod.cmds['<?php echo $attribute['attribute_id']; ?>'] = function(){

                                                       };

                                                       jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').click(smartexprod.cmds['<?php echo $attribute['attribute_id']; ?>']);
                                                       jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').click(function(){
                                                           smartChangeProductPrice();  
                                                           smartValidateAll();
                                                       });
                                                   });
                                                </script>


                                                <?php
                                            break;

                                            case 'text':
                                                ?>

                                                <?php 
                                                  if(isset($attrib_label[1]) && !empty($attrib_label[1])){
                                                      ?>
                                                      <p style="padding:0px; margin:0px;" class="smartex-beforelinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[1]);
                                                        ?>
                                                      </p>
                                                      <?php
                                                  }
                                                ?>

                                                <input <?php if(isset($attribute['config']['camera'])){ ?>data-threedcameradata ="<?php echo $attribute['config']['camera']; ?>" <?php } ?>
                                                        maxlength="<?php echo $attribute['config']['maxmin']['max']; ?>"
                                                        value="<?php echo $attribute['config']['default']; ?>"
                                                        <?php if(!in_array($attribute['attribute_id'],$ignoredPrices)
                                                                  && floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                        onkeyup="if(jQuery(this).val().length > 0){
                                                                   jQuery('#smartex-text-price-<?php echo $attribute['attribute_id']; ?>').css('display','block');
                                                                   <?php
                                                                    switch($attribute['config']['price']['behavior']){
                                                                        case 'd':
                                                                            ?>
                                                                             jQuery('#smartex-text-price-text-<?php echo $attribute['attribute_id']; ?>').html(String(jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').val().length * parseFloat(<?php echo $attribute['config']['price']['amount']; ?>)));
                                                                            <?php
                                                                            break;
                                                                        case 's':
                                                                            ?>
                                                                             jQuery('#smartex-text-price-text-<?php echo $attribute['attribute_id']; ?>').html('<?php echo $attribute['config']['price']['amount']; ?>');
                                                                            <?php
                                                                            break;
                                                                   }
                                                                   ?>
                                                               }else{
                                                                   jQuery('#smartex-text-price-<?php echo $attribute['attribute_id']; ?>').css('display','none');                                                               
                                                                   jQuery('#smartex-text-price-text-<?php echo $attribute['attribute_id']; ?>').html('');
                                                               }"
                                                      <?php } ?>
                                                        name="<?php echo $attribute['attribute_id']; ?>"
                                                        type="text"
                                                        class="<?php if($attribute['config']['required']=== '1'){ ?> required-entry <?php } ?> smartex-text"
                                                        id="smartex-<?php echo $attribute['attribute_id']; ?>"
                                                            <?php
                                                            foreach($attribute['config'] as $configkey => $configval){
                                                                if(is_array($configval)){
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>='<?php echo json_encode($configval); ?>'
                                                                    <?php
                                                                }
                                                                else{
                                                                    ?>
                                                                        data-conf-<?php echo $configkey ?>="<?php echo $configval; ?>"
                                                                    <?php
                                                                }
                                                            }
                                                        ?> 
                                                />

                                                <?php 
                                                if(isset($attrib_label[2]) && !empty($attrib_label[2])){
                                                    ?>
                                                    <span class="smartex-behindinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[2]);
                                                        ?>
                                                    </span>
                                                    <?php
                                                }

                                                if(isset($attrib_label[3]) && !empty($attrib_label[3])){
                                                    ?>
                                                    <p style="padding:0px; margin:0px;" class="smartex-afterinput">
                                                        <?php
                                                        echo Mage::helper('cms')->getPageTemplateProcessor()->filter($attrib_label[3]);
                                                        ?>
                                                    </p>
                                                    <?php
                                                }

                                                if(isset($attribute['config']['maxmin']['max']) && intval($attribute['config']['maxmin']['max']) > 0){
                                                    ?>
                                                    <p style="padding:0px; margin:0px;">
                                                        <?php
                                                        echo Mage::helper('picaris')->__('Maximum length is:').strval($attribute['config']['maxmin']['max']);
                                                        ?>
                                                    </p>
                                                    <?php
                                                }
                                                ?>

                                                <?php if(floatval($attribute['config']['price']['amount']) > 0){ ?>
                                                    <span id="smartex-text-price-<?php echo $attribute['attribute_id']; ?>" <?php if($attribute['config']['default']!== '1'){ ?>style="display: none;"<?php } ?>>
                                                        +<span id="smartex-text-price-text-<?php echo $attribute['attribute_id']; ?>">
                                                            <?php echo $attribute['config']['price']['amount']; ?>
                                                        </span><?php echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); ?>
                                                    </span>
                                                <?php } ?>

                                                <script type="text/javascript">
                                                   jQuery(document).ready(function(){
                                                       smartexprod.cmds['<?php echo $attribute['attribute_id']; ?>']  = function(){

                                                       };

                                                       jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').keyup(smartexprod.cmds['<?php echo $attribute['attribute_id']; ?>']);
                                                       jQuery('#smartex-<?php echo $attribute['attribute_id']; ?>').keyup(function(){
                                                            smartChangeProductPrice();  
                                                            smartValidateAll();
                                                       });
                                                   });
                                                </script>

                                                <?php
                                            break;

                                            default:
                                            ?>
                                               <script type="text/javascript">
                                                    alert('<?php echo $attribute['attribute_id'] ?> has an invalid type of <?php echo $attribute['type']; ?>');
                                               </script>
                                            <?php
                                               die();
                                    }
                                    ?>
                                    </div>
                                    <!-- attribute content end -->
                                    
                                </li>
                                <?php
                            }
                            ?>
                            <!-- attribute load end -->
                        </ul>
                    </li>
                </ul>
            <!-- group load up end-->
            <?php
            }
            ?>
            
            <?php
            foreach($custdata as $custoption){
                ?>
            
                <?php
                foreach($custoption['attributes'] as $attribute){
                    ?>

                    <!-- dependency script start -->
                    <script type="text/javascript">
                        jQuery(document).ready(function(){
                            <?php
                            foreach($attribute['dependencies'] as $dependency){
                                foreach($dependency['config']['depender'] as  $depender){
                                    ?>
                                    <?php if(intval($dependency['visible_dependers']) === 0){ ?>    
                                        jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                    <?php } ?>   
                                    jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disable');                    
                                    <?php
                                }

                                //dropdown with option dependency 
                                if($dependency['config']['dependee']['type']==='dropdown' && isset($dependency['config']['dependeeOptionDependency'])){
                                    ?>
                                         jQuery('#smartex-<?php echo $dependency['dependee'] ?>').change(function(){
                                             var dependeeOptions = <?php echo json_encode($dependency['config']['dependeeOptionDependency']); ?>;
                                             if(dependeeOptions.indexOf(jQuery(this).val()) > -1){
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                           jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                           jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                             else{
                                                <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            <?php if(intval($dependency['visible_dependers']) === 0){ ?> 
                                                               jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                            <?php } ?>
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disable');
                                                            switch(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                case 'text':
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                    break;
                                                                case 'checkbox':
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                        }
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                    break;
                                                                default:
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                            if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                            }
                                                                            else{
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                            }
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.smartex-default-image').trigger('click');
                                                                        }
                                                                    break;
                                                            }
                                                        }

                                                     <?php
                                                 }
                                                 ?>
                                             }
                                         });
                                    <?php
                                }

                                //dropdown without option dependency
                                elseif($dependency['config']['dependee']['type']==='dropdown' && !isset($dependency['config']['dependeeOptionDependency'])){
                                    ?>
                                         jQuery('#smartex-<?php echo $dependency['dependee'] ?>').change(function(){
                                             if(jQuery(this).val().length > 0){
                                                <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                             else{
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        <?php if(intval($dependency['visible_dependers']) === 0){ ?> 
                                                            jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                        <?php } ?>
                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                        switch(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                            case 'text':
                                                                    jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                    jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                break;
                                                            case 'checkbox':
                                                                    if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                    }
                                                                    else{
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                    }
                                                                    jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                break;
                                                            default:
                                                                    if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                        }
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                    }
                                                                    else{
                                                                        jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                        jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.smartex-default-image').trigger('click');
                                                                    }
                                                                break;
                                                        }

                                                     <?php
                                                 }
                                                 ?>
                                             }
                                         });   
                                    <?php
                                }

                                //image with option dependency 
                                if($dependency['config']['dependee']['type']==='image' && isset($dependency['config']['dependeeOptionDependency'])){
                                    ?>
                                         jQuery('.smartex-image-input-<?php echo $dependency['dependee'] ?>').click(function(){
                                             var dependeeOptions = <?php echo json_encode($dependency['config']['dependeeOptionDependency']); ?>;
                                             if(dependeeOptions.indexOf(jQuery(this).attr('data-optionid')) > -1){
                                                 <?php
                                                 foreach($dependency['config']['depender'] as $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                            
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                             else{
                                                <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                               jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                            <?php } ?>
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disable');

                                                            switch(jQuery('#<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                case 'text':
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                    break;
                                                                case 'checkbox':
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                        }
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                    break;
                                                                default:
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                            if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                            }
                                                                            else{
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                            }
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.smartex-default-image').trigger('click');
                                                                            //1902kelvin-recopy
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-price-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                        }
                                                                    break;
                                                            }
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                         });
                                    <?php
                                }

                                //image without option dependency
                                elseif($dependency['config']['dependee']['type']==='image' && !isset($dependency['config']['dependeeOptionDependency'])){
                                    ?>
                                         jQuery('.smartex-image-input-<?php echo $dependency['dependee'] ?>').click(function(){
                                             if(jQuery(this).attr('data-optionid').length > 0){
                                                <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                            
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                             else{
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                                jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                            <?php } ?>
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                            switch(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                case 'text':
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                    break;
                                                                case 'checkbox':
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                        }
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                    break;
                                                                default:
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                            if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                            }
                                                                            else{
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                            }
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.smartex-default-image').trigger('click');
                                                                            //1902kelvin-recopy
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-price-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                                        }
                                                                    break;
                                                            }
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                         });   
                                    <?php
                                }

                                //checkbox
                                elseif($dependency['config']['dependee']['type']==='checkbox'){
                                    ?>
                                         jQuery('#smartex-<?php echo $dependency['dependee'] ?>').click(function(){
                                             if(jQuery('#smartex-<?php echo $dependency['dependee'] ?>:checked').length === 1){
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                             else{
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                               jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                            <?php } ?>
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                            switch(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                case 'text':
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                    break;
                                                                case 'checkbox':
                                                                       if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                        }
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                    break;
                                                                default:
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                            if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                            }
                                                                            else{
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                            }
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.smartex-default-image').trigger('click');
                                                                        }
                                                                    break;
                                                            }
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                         });   
                                    <?php
                                }

                                //text
                                elseif($dependency['config']['dependee']['type']==='text'){
                                    ?>
                                         jQuery('#smartex-<?php echo $dependency['dependee'] ?>').keyup(function(){
                                             if(jQuery(this).val().length > 0){
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'block'});
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').removeAttr('disabled');
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                             else{
                                                 <?php
                                                 foreach($dependency['config']['depender'] as  $depender){
                                                     ?>
                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').length){
                                                            <?php if(intval($dependency['visible_dependers']) === 0){ ?>
                                                                jQuery('#smartex-li-<?php echo $depender[0]['attribute_id']; ?>').css({'display':'none'});
                                                            <?php } ?>
                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('disabled','disabled');

                                                            switch(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('type')){
                                                                case 'text':
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default'));
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('keyup');
                                                                    break;
                                                                case 'checkbox':
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-conf-default') === '1'){
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',true);
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('checked',false);
                                                                        }
                                                                        jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('click');
                                                                    break;
                                                                default:
                                                                        if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').prop('tagName').toLowerCase() === 'select'){
                                                                            if(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').length){
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').find('option[selected]').prop("selected", true);
                                                                            }
                                                                            else{
                                                                                jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?> option:first').prop("selected", true);
                                                                            }
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').trigger('change');
                                                                        }
                                                                        else{
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?> .smartex-image-input-<?php echo $depender[0]['attribute_id']; ?>').css('border','none');
                                                                            jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').val(jQuery('#smartex-<?php echo $depender[0]['attribute_id']; ?>').attr('data-defaultvalue'));
                                                                            jQuery('#smartex-imagelist-<?php echo $depender[0]['attribute_id']; ?>').find('.smartex-default-image').trigger('click');
                                                                        }
                                                                    break;
                                                            }
                                                        }
                                                     <?php
                                                 }
                                                 ?>
                                             }
                                         });   
                                    <?php
                                }
                            }
                            ?>
                        });                                                                  
                    </script>
                    <!-- dependency script end -->
                    <?php
                }
            }
            ?>
         
        </div>
        
    </div>
    <div class="smart-footer" onclick="if(smartexprod.currentlevel === 1){jQuery('#smart-submit-btn').trigger('click');}">
        <a href="#">
            <?php  if($tproduct->getStockItem()->getIsInStock()){ ?>
                <span class="smart-footer-buy-text">
                    <?php echo Mage::helper('picaris')->__('Buy'); ?>
                </span>
                <span class="smart-footer-price-text">
                    (<?php echo Mage::helper('core')->currency($tproduct->getPrice(), true, false); ?>)        
                </span>
            <?php }
            else{
                ?>
                <span class="smart-footer-buy-text">
                    <?php echo Mage::helper('picaris')->__('Out of Stock'); ?>
                </span>
                <span class="smart-footer-price-text">
                    (<?php echo Mage::helper('core')->currency($tproduct->getPrice(), true, false); ?>)        
                </span>
            <?php
            }
            ?>
        </a>
    </div>
    <?php 
    if($tproduct->getStockItem()->getIsInStock()){
        ?>
        <input style="display:none;" type="submit" id="smart-submit-btn" disabled="disabled">
        <?php
    }
    else{
        ?>
        <input style="display:none;" type="button" id="smart-submit-btn" disabled="disabled">
        <?php 
    }
    ?>
</form>